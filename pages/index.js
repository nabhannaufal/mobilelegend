import axios from "axios";
import Link from "next/link";
import styles from "../styles/Home.module.css";

export default function Home({ data }) {
  return (
    <>
      <h1>List Hero Mobile Legend</h1>
      <div>
        {data.map((item, index) => {
          return (
            <>
              <Link href={`${item.hero_id}`} key={index} passHref>
                <div className={styles.card} key={index}>
                  <h2>{item.hero_name}</h2>
                </div>
              </Link>
            </>
          );
        })}
      </div>
    </>
  );
}

export const getStaticProps = async () => {
  const daftarhero = await axios.get(
    "https://api.dazelpro.com/mobile-legends/hero"
  );
  const { data } = daftarhero;
  return {
    props: {
      data: data.hero,
    },
  };
};
